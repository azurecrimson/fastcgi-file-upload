use age::keys::RecipientKey;
use clap::{App, Arg};
use htmlescape::encode_minimal;
use log::{set_max_level, LevelFilter, debug, error, info, trace, warn};
use nom::sequence::tuple;
use nom::{alt, bytes, character, tag, take, IResult};
use regex::Regex;
use rusqlite::{named_params, Connection, NO_PARAMS};
use sha2::{Digest, Sha256};
use uuid::Uuid;

use std::collections::HashMap;
use std::error::Error;
use std::fs::{read_dir, remove_file, DirBuilder, File};
use std::io::{ErrorKind, Read, Write};
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;

enum UploadStatus {
    Writing,
    Deleting,
    Error,
    Finished,
    Removed,
}
impl UploadStatus {
    fn value(&self) -> String {
        return match *self {
            UploadStatus::Writing => "Writing",
            UploadStatus::Deleting => "Deleting",
            UploadStatus::Error => "Error",
            UploadStatus::Finished => "Finished",
            UploadStatus::Removed => "Removed",
        }
        .to_string();
    }
}

fn extract_boundary(s: &str) -> IResult<&str, String> {
    trace!("extract_boundary");
    let mime_type = bytes::complete::tag("multipart/form-data");
    let sep = bytes::complete::take_while1(|c| c == ' ' || c == ';');
    let boundary_tag = bytes::complete::tag("boundary=");
    let boundary = bytes::complete::take_while1(|c| c != ';' && c != ' ');

    let (input, (_, _, _, boundary)) = tuple((mime_type, sep, boundary_tag, boundary))(s)?;

    return Ok((input, boundary.to_string()));
}
fn extract_filename<'a>(s: &'a str, boundary: &str) -> IResult<&'a str, String> {
    let edge = bytes::streaming::tag("--");
    let boundary = bytes::streaming::tag(boundary);
    let nl = character::streaming::crlf;
    let optnl = nom::combinator::opt(nl);
    let disposition = bytes::streaming::tag("Content-Disposition: form-data");
    let sep = bytes::streaming::is_a(" ;");
    let name = bytes::streaming::tag("name=\"data\"");
    let filename_tag = bytes::streaming::tag("filename=");
    let quote = character::streaming::char('\"');
    let filename = bytes::streaming::escaped_transform(
        character::streaming::none_of("\"\r\n\\"),
        '\\',
        |i: &str| alt!(i, tag!("\"") => {|_| "\""} | take!(1) => {|c| c}),
    );
    let content_type = bytes::streaming::tag("Content-Type: ");
    let mime_type = bytes::streaming::take_while1(|c: char| {
        character::is_alphanumeric(c as u8) || c == '/' || c == '-'
    });

    let (s, (_, _, _, _, _, _, _, _, _, _, filename, _, _, _, _, _, _)) = tuple((
        optnl,
        edge,
        boundary,
        nl,
        disposition,
        &sep,
        name,
        &sep,
        filename_tag,
        &quote,
        filename,
        &quote,
        nl,
        content_type,
        mime_type,
        nl,
        nl,
    ))(s)?;

    return Ok((&s, filename.to_string()));
}
fn validate_footer<'a>(s: &'a str, boundary: &str) -> IResult<&'a str, String> {
    let boundary = bytes::streaming::tag(boundary);
    let edge = bytes::streaming::tag("--");
    let nl = character::streaming::crlf;

    let (s, boundary) = boundary(s)?;
    let (s, _) = edge(s)?;
    let (s, _) = nl(s)?;

    return Ok((&s, boundary.to_string()));
}

fn send_failure<W: Write>(response: &mut W) {
    let content =
        b"<!doctype html>\n<html>\n<head>\n<title>Upload</title>\n<meta charset='utf-8' />
        <style>body{filter: invert() hue-rotate(180deg); background: black}</style>
        </head>\n<body>
        <h1>Upload Failed</h1>
        </body>\n</html>";
    match response.write_all(content) {
        Ok(_) => (),
        Err(e) => warn!("{:?}", e),
    };
}
fn send_status<W: Write>(
    response: &mut W,
    files: &Vec<Result<(String, String), (String, String)>>,
) {
    let header = "<!doctype html>\n<html>\n<head>\n<title>Upload</title>\n<meta charset='utf-8' />
        <style>
        body { color: #e6e6e6; background: black; }
        table { border-collapse: collapse; border-spacing: 0; }
        td { border: 1px solid; padding: 0.5em; }
        th { border: 1px solid; padding: 0.5em; }
        </style>
        </head>\n<body>
        <h1>Upload Completed</h1>
        <table><tr><th>Filename</th><th>SHA256</th></tr>";
    let footer = "</table></body>\n</html>";
    let mut content = header.to_string();
    for file in files {
        let row = match file {
            Ok(v) => format!("<tr><td>{}</td><td>{}</td></tr>", encode_minimal(&v.0), v.1),
            Err(e) => format!(
                "<tr><td>{}</td><td>{}</td></tr>",
                encode_minimal(&e.0),
                "ERROR"
            ),
        };
        content.push_str(&row);
    }
    content.push_str(footer);
    match response.write_all(content.as_bytes()) {
        Ok(_) => (),
        Err(e) => warn!("{:?}", e),
    };
}
fn send_upload_page<W: Write>(response: &mut W) {
    let content =
        b"<!doctype html>\n<html>\n<head>\n<title>Upload</title>\n<meta charset='utf-8' />
        <style>body{filter: invert() hue-rotate(180deg); background: black}</style>
        </head>\n<body>
        <form action='/upload' method='post' enctype='multipart/form-data'>
            <input type='file' name='data' multiple><br><br>
            <input type='submit' value='Upload Files'>
        </form>
        </body>\n</html>";
    match response.write_all(content) {
        Ok(_) => (),
        Err(e) => warn!("{:?}", e),
    };
}

struct PostReader<T: Read> {
    rdr: T,
    bnd: Vec<u8>,
    pmt: Vec<isize>,
    idx: isize,
    buf: Vec<u8>,
}
impl<T: Read> PostReader<T> {
    pub fn new(reader: T, boundary: Vec<u8>) -> PostReader<T> {
        // Knuth-Morris-Pratt string searching algorithm
        let mut c: isize = 0;
        let mut table: Vec<isize> = vec![0; boundary.len()];
        table[0] = -1;
        for i in 0..table.len() {
            if boundary[i] == boundary[c as usize] {
                table[i] = table[c as usize];
            } else {
                table[i] = c;
                c = table[c as usize];
                while c >= 0 && boundary[i] != boundary[c as usize] {
                    c = table[c as usize];
                }
            }
            c += 1;
        }
        let partial_match_table = table;

        return PostReader {
            rdr: reader,
            bnd: boundary,
            pmt: partial_match_table,
            idx: 0,
            buf: Vec::new(),
        };
    }
    fn string_search(
        table: &mut Vec<isize>,
        idx: &mut isize,
        bnd: &Vec<u8>,
        b: u8,
    ) -> Result<bool, String> {
        let mut i = *idx;
        if *bnd.get(i as usize).ok_or("KMP invalid index")? == b {
            i += 1;
            if i == (bnd.len() as isize) - 1 {
                *idx = i;
                return Ok(true);
            }
        } else {
            i = *table.get(i as usize).ok_or("KMP invalid index")?;
            if i < 0 {
                i += 1;
            }
        }
        *idx = i;
        return Ok(false);
    }
    fn refill_buf(&mut self, limit: usize) -> Result<usize, String> {
        let mut tmp = vec![0; limit];
        let mut count = 0;
        while self.buf.len() < limit {
            let n = match self.rdr.read(&mut tmp) {
                Ok(n) => n,
                Err(ref e) if e.kind() == ErrorKind::Interrupted => continue,
                Err(e) => return Err(format!("{:#?}", e)),
            };

            self.buf.extend_from_slice(&tmp.split_at(n).0);
            count += n;
            if n == 0 {
                break;
            }
        }
        return Ok(count);
    }
    fn output_buf_till(&mut self, idx: usize) -> Vec<u8> {
        let (ret, right) = self.buf.split_at(idx + 1);
        let ret = ret.to_vec();
        self.buf = right.to_vec();
        return ret.to_vec();
    }
    pub fn read_until_newlines(&mut self, limit: usize) -> Result<Vec<u8>, String> {
        trace!("read_until_newlines");
        const END: &[u8] = b"\r\n\r\n";

        while 0 != self.refill_buf(limit)? {}

        for (i, b) in self.buf.iter().enumerate() {
            let fnd = PostReader::<T>::string_search(&mut self.pmt, &mut self.idx, &self.bnd, *b)?;

            if i >= (self.bnd.len() - 2) {
                let j = i - (self.bnd.len() - 2);
                if j > END.len() {
                    let check = self.buf.get((j - END.len())..j).unwrap();
                    if check == END {
                        return Ok(self.output_buf_till(j - 1));
                    }
                }
            }
            if fnd {
                return Err("Found boundary before header was read".to_string());
            }
        }

        return Err("Header not found before limit or EOF".to_string());
    }
    pub fn read_until_boundary(&mut self, buf_size: usize) -> Result<(Vec<u8>, bool), String> {
        trace!("read");

        while 0 != self.refill_buf(buf_size)? {}
        for (i, b) in self.buf.iter().enumerate() {
            let fnd = PostReader::<T>::string_search(&mut self.pmt, &mut self.idx, &self.bnd, *b)?;

            if i >= (self.bnd.len() - 1) {
                let j = i - (self.bnd.len() - 1);
                if fnd {
                    return Ok((self.output_buf_till(j), true));
                }
            }
        }

        if self.buf.len() >= self.bnd.len() {
            let j = self.buf.len() - self.bnd.len();
            return Ok((self.output_buf_till(j), false));
        }
        return Err("Reached EOF without finding boundary".to_string());
    }
    pub fn is_footer(&mut self, limit: usize) -> Result<bool, String> {
        while self.refill_buf(limit)? > 0 {}
        match validate_footer(
            &String::from_utf8_lossy(&self.buf),
            &String::from_utf8_lossy(&self.bnd),
        ) {
            Ok(_) => return Ok(true),
            Err(_) => return Ok(false),
        };
    }
}

fn write_upload_to_disk<T: Read>(
    rdr: &mut PostReader<T>,
    key: &Vec<RecipientKey>,
    dir: &PathBuf,
    uuid: &String,
) -> Result<String, String> {
    const BUF_SIZE: usize = 0x10000;

    let file = dir.join(uuid);
    let file = File::create(file).or(Err("Failed to create file for upload".to_string()))?;

    let mut hasher = Sha256::new();

    let encryptor = age::Encryptor::Keys(key.clone());
    let mut writer = encryptor
        .wrap_output(file, age::Format::Binary)
        .or(Err("Could not open age writer".to_string()))?;
    loop {
        let (buf, done) = rdr.read_until_boundary(BUF_SIZE)?;
        writer
            .write_all(&buf)
            .or(Err("Error while writing file".to_string()))?;
        hasher.input(&buf);
        if done {
            break;
        }
    }

    writer.finish().or(Err("Error writing age footer"))?;
    return Ok(hex::encode(hasher.result()));
}

fn handle_post<T: Read>(
    request: &mut T,
    content_type: &str,
    data: &Router,
) -> Result<Vec<Result<(String, String), (String, String)>>, String> {
    let (_, boundary) = match extract_boundary(content_type) {
        Ok(v) => v,
        Err(e) => return Err(format!("Parse failure, {:#?}", e)),
    };

    let mut rdr = PostReader::new(
        request,
        ("\r\n--".to_owned() + &boundary).as_bytes().to_vec(),
    );

    let mut results = Vec::new();
    loop {
        const METADATA_LIMIT: usize = 0x1000;
        let header = rdr.read_until_newlines(METADATA_LIMIT)?;
        let (_, filename) = match extract_filename(&String::from_utf8_lossy(&header), &boundary) {
            Ok(v) => v,
            Err(e) => {
                return Err(format!(
                    "Header decode failed {:#?}\n\nHeader: {:?}",
                    e,
                    String::from_utf8_lossy(&header)
                ))
            }
        };

        let mut db = data.db.lock().unwrap();
        let uuid = Uuid::new_v4().to_hyphenated().to_string();
        data.uuids.lock().unwrap().push(uuid.clone());
        match db.execute_named(
            "INSERT INTO files (uuid, filename, state) VALUES (:uuid, :filename, :state)",
            named_params! {
                ":uuid": uuid,
                ":filename": filename,
                ":state": UploadStatus::Writing.value(),
            },
        ) {
            Ok(_) => (),
            Err(e) => {
                // reset the database
                std::mem::replace(&mut *db, db_init(&data.dir));
                return Err(format!("Unable to insert database entry {:?}", e));
            }
        };
        std::mem::drop(db);

        let result = write_upload_to_disk(&mut rdr, &data.key, &data.dir, &uuid);

        let state = match result {
            Ok(_) => UploadStatus::Finished,
            Err(_) => UploadStatus::Error,
        };
        let mut db = data.db.lock().unwrap();
        match db_update_state(&db, &uuid, &state) {
            Ok(_) => (),
            Err(e) => {
                // reset the database
                std::mem::replace(&mut *db, db_init(&data.dir));
                return Err(format!(
                    "Unable to set upload completion state in database {:?}",
                    e
                ));
            }
        };
        std::mem::drop(db);

        const FOOTER_LIMIT: usize = 0x1000;
        let done = rdr.is_footer(FOOTER_LIMIT)?;

        match result {
            Ok(hash) => results.push(Ok((filename, hash))),
            Err(e) => results.push(Err((filename, e))),
        };

        if done {
            break;
        } else {
            continue;
        }
    }

    return Ok(results);
}

fn handle<R: Read, W: Write>(post: bool, content_type: String, request: &mut R, response: &mut W, router: &Router) {
    if post {
        debug!("Processing post request");
        match handle_post(request, &content_type, router) {
            Ok(v) => return send_status(response, &v),
            Err(e) => {
                warn!("{}", e);
                return send_failure(response);
            }
        };
    } else {
        request.read(&mut [0]).unwrap_or(0);
        debug!("Sending upload form");
        send_upload_page(response);
        debug!("Upload form sent");
    }
}

#[derive(Clone)]
struct Router {
    tx: mpsc::Sender<thread::ThreadId>,
    dir: Arc<PathBuf>,
    key: Arc<Vec<RecipientKey>>,
    uuids: Arc<Mutex<Vec<String>>>,
    db: Arc<Mutex<Connection>>,
}
impl Router {
    fn new(
        tx: mpsc::Sender<thread::ThreadId>,
        dir: Arc<PathBuf>,
        keys: Arc<Vec<RecipientKey>>,
        db: Arc<Mutex<Connection>>,
    ) -> Self {
        return Router {
            tx: tx,
            dir: dir,
            key: keys,
            uuids: Arc::new(Mutex::new(Vec::new())),
            db: db,
        };
    }
}
impl Drop for Router {
    fn drop(&mut self) {
        if thread::panicking() {
            let id = thread::current().id();
            match self.tx.send(id) {
                Ok(_) => (),
                Err(e) => error!("MPSC send failed {:#?}", e),
            };

            let mut db = self.db.lock().unwrap();
            let uuids = self.uuids.lock().unwrap();
            for uuid in &*uuids {
                match db_update_state(&db, &uuid, &UploadStatus::Error) {
                    Ok(_) => (),
                    Err(e) => {
                        // reset the database
                        std::mem::replace(&mut *db, db_init(&self.dir));
                        return warn!("Unable to set upload completion state in database {:?}", e);
                    }
                }
            }
        }
    }
}
impl gfcgi::Handler for Router {
    fn process(&self, request: &mut gfcgi::Request, response: &mut gfcgi::Response) {
        // Content-Type: text/html; charset=UTF-8\n\n
        response.header_utf8("Content-type", "text/html; charset=UTF-8");

        let post = request.header_utf8(b"REQUEST_METHOD") == Some("POST");
        let content_type = match request.header_utf8(b"CONTENT_TYPE") {
            Some(v) => v.to_string(),
            None => return send_failure(response),
        };
        handle(post, content_type, request, response, self);
    }
}

fn db_init(dir: &PathBuf) -> Connection {
    DirBuilder::new()
        .recursive(true)
        .create(&dir)
        .expect("Could not create dir");
    let db = dir.join("upload.db3");
    let db = Connection::open(db).expect("Failed to open database");
    db.execute(
        "CREATE TABLE IF NOT EXISTS files (
        uuid TEXT PRIMARY KEY,
        filename TEXT,
        state TEXT,
        ctime DATETIME DEFAULT CURRENT_TIMESTAMP,
        mtime DATETIME DEFAULT CURRENT_TIMESTAMP
        )",
        NO_PARAMS,
    )
    .expect("Unable to create database table");
    return db;
}
fn db_update_state(db: &Connection, uuid: &str, state: &UploadStatus) -> Result<usize, Box<dyn Error>> {
    return db.execute_named(
        "UPDATE files SET state = :state, mtime = CURRENT_TIMESTAMP WHERE uuid = :uuid",
        named_params! {
            ":uuid": uuid,
            ":state": state.value(),
        },
    ).map_err(|e| e.into());
}
fn db_delete_row(db: &Connection, uuid: &str) -> Result<usize, Box<dyn Error>> {
    return db.execute_named(
        "DELETE FROM files WHERE uuid = :uuid",
        named_params! {
            ":uuid": &uuid,
        },
    ).map_err(|e| e.into());
}

fn main() {
    let matches = App::new("Upload")
        .about("Fastcgi program receives files and encrypts them to an age recipient")
        .arg(
            Arg::with_name("dir")
                .help("Location of upload directory")
                .required(true)
                .takes_value(true),
        )
        .subcommand(
            App::new("run")
                .arg(
                    Arg::with_name("listener")
                        .short("l")
                        .long("listener")
                        .help("TCP listener location")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("threads")
                        .short("t")
                        .long("threads")
                        .help("Number of listener threads")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("keys")
                        .short("k")
                        .long("keys")
                        .help("Age (https://github.com/FiloSottile/age/) recipient key")
                        .takes_value(true)
                        .use_delimiter(true),
                ),
        )
        .subcommand(
            App::new("print")
                .arg(
                    Arg::with_name("age")
                        .short("a")
                        .long("age")
                        .help("Print age commands to extract files, takes keyfile path")
                        .takes_value(true),
                )
        )
        .subcommand(
            App::new("remove")
                .arg(
                    Arg::with_name("state")
                        .short("s")
                        .long("state")
                        .help("Remove entries and files with this state")
                        .takes_value(true)
                        .use_delimiter(true),
                )
                .arg(
                    Arg::with_name("uuid")
                        .short("u")
                        .long("uuid")
                        .help("Remove entries and files with this uuid")
                        .takes_value(true)
                        .use_delimiter(true),
                )
                .arg(
                    Arg::with_name("unindexed")
                        .long("unindexed")
                        .help("Delete file if database entry is missing"),
                )
                .arg(
                    Arg::with_name("missing")
                        .long("missing")
                        .help("Delete database entry if file is missing"),
                )
                .arg(
                    Arg::with_name("markonly")
                        .long("mark-only")
                        .help("Don't actually delete database entries"),
                ),
        )
        .subcommand(
            App::new("extract")
                .arg(
                    Arg::with_name("out")
                        .short("o")
                        .long("out")
                        .help("File will be extracted here")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("age")
                        .short("a")
                        .long("age")
                        .help("Print age commands to extract files, takes keyfile path")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("uuid")
                        .short("u")
                        .long("uuid")
                        .help("Remove entries and files with this uuid")
                        .takes_value(true)
                        .use_delimiter(true),
                )
                .arg(Arg::with_name("use_filename")
                    .short("a")
                    .long("append")
                    .help("",
                )),
        )
        .get_matches();

    simple_logger::init().unwrap();
    set_max_level(LevelFilter::Debug);

    let dir = Arc::new(PathBuf::from(
        matches.value_of("dir").expect("Could not get dir argument"),
    ));

    info!("Started");
    if let Some(ref matches) = matches.subcommand_matches("run") {
        let keys = Arc::new(
            matches
                .values_of("keys")
                .unwrap()
                .map(|key| match RecipientKey::from_str(key) {
                    Ok(v) => v,
                    Err(e) => panic!(format!("{:?}, key={:?}", e, key)),
                })
                .collect::<Vec<RecipientKey>>(),
        );

        let listener = matches
            .value_of("listener")
            .expect("Couldn't get listener value");
        let client = gfcgi::Client::new(listener.clone());

        let threads = matches
            .value_of("threads")
            .expect("Couldn't get thread count");
        let threads = threads.parse::<u32>().expect("Invalid thread count");

        let db = Arc::new(Mutex::new(db_init(&dir)));

        let (tx, rx) = mpsc::channel();
        let mut workers = HashMap::new();
        for _ in 0..threads {
            let handle = client.run(Router::new(
                tx.clone(),
                dir.clone(),
                keys.clone(),
                db.clone(),
            ));
            workers.insert(handle.thread().id(), handle);
        }

        loop {
            let id = match rx.recv() {
                Ok(v) => v,
                Err(e) => return error!("MPSC {:#?}", e),
            };

            workers.remove(&id);
            info!("Restarting worker {:?}", id);
            let handle = client.run(Router::new(
                tx.clone(),
                dir.clone(),
                keys.clone(),
                db.clone(),
            ));
            workers.insert(handle.thread().id(), handle);
        }
    } else if let Some(ref matches) = matches.subcommand_matches("remove") {
        let db = dir.join("upload.db3");
        let db = Connection::open(db).expect("Failed to create database");

        let uuids: Vec<&str>;
        if matches.is_present("uuid") {
            uuids = matches.values_of("uuid").unwrap().collect();
        } else {
            uuids = Vec::new();
        }
        for uuid in uuids {
                let mut stmt = db
                    .prepare("SELECT uuid, state FROM files WHERE uuid = :uuid")
                    .expect("Failed to prepare SELECT statement");
                let mut rows = stmt
                    .query_named(named_params! {
                        ":uuid": &uuid,
                    })
                    .expect("Error querying rows");
                let state: String = match rows.next().expect("Error reading rows") {
                    Some(v) => v.get(1).expect("Error getting state"),
                    None => {warn!("UUID {} not found", &uuid); continue;},
                };

                let file = dir.join(&uuid);

                db_update_state(&db, &uuid, &UploadStatus::Deleting).expect("Error setting deleting state");
                let file_successful = remove_file(&file).is_ok();

                if file_successful {
                    if matches.is_present("markonly") {
                        db_update_state(&db, &uuid, &UploadStatus::Removed).expect("Error setting removed state");
                    } else {
                        db_delete_row(&db, &uuid).expect("Error deleting row");
                    }
                } else {
                    warn!("Unable to delete file {}, resetting", &uuid);
                    db.execute_named(
                        "UPDATE files SET state = :state WHERE uuid = :uuid",
                        named_params! {
                            ":uuid": &uuid,
                            ":state": &state,
                        },
                    )
                    .expect("Error resetting state");
                }
        }

        let states: Vec<&str>;
        if matches.is_present("state") {
            states = matches.values_of("state").unwrap().collect();
        } else {
            states = Vec::new();
        }
        for state in states {
            let mut stmt = db
                .prepare("SELECT uuid, state FROM files WHERE state = :state")
                .expect("Failed to prepare SELECT statement");
            let mut rows = stmt
                .query_named(named_params! {
                    ":state": &state,
                })
                .expect("Error querying rows");
            while let Some(row) = rows.next().expect("Error reading rows") {
                let uuid: String = row.get(0).expect("Error getting uuid");
                let file = dir.join(&uuid);

                db_update_state(&db, &uuid, &UploadStatus::Deleting).expect("Error setting deleting state");
                let file_successful = remove_file(&file).is_ok();

                if file_successful {
                    if matches.is_present("markonly") {
                        db_update_state(&db, &uuid, &UploadStatus::Removed).expect("Error setting removed state");
                    } else {
                        db_delete_row(&db, &uuid).expect("Error deleting row");
                    }
                } else {
                    warn!("Unable to delete file {}, resetting", &uuid);
                    db.execute_named(
                        "UPDATE files SET state = :state WHERE uuid = :uuid",
                        named_params! {
                            ":uuid": &uuid,
                            ":state": &state,
                        },
                    )
                    .expect("Error resetting state");
                }
            }
        }

        // delete entries with missing files
        if matches.is_present("missing") {
            let mut stmt = db
                .prepare("SELECT uuid FROM files")
                .expect("Failed to prepare SELECT statement");
            let mut rows = stmt.query(NO_PARAMS).expect("Error querying rows");
            while let Some(row) = rows.next().expect("Error reading rows") {
                let uuid: String = row.get(0).expect("Error getting uuid");
                let file = dir.join(&uuid);
                if !file.is_file() {
                    if matches.is_present("markonly") {
                        db_update_state(&db, &uuid, &UploadStatus::Removed).expect("Error setting deleted state");
                    } else {
                        db_delete_row(&db, &uuid).expect("Error deleting row");
                    }
                }
            }
        }

        // delete files with missing entries
        if matches.is_present("unindexed") {
            let entries = read_dir(dir.as_ref()).expect("Could not list contents of dir");
            for entry in entries {
                let entry = match entry {
                    Ok(v) => v,
                    Err(_) => continue,
                };
                let re = Regex::new(r"^[[:xdigit:]]{8}\-([[:xdigit:]]{4}\-){3}[[:xdigit:]]{12}$")
                    .unwrap();
                let filename = match entry.file_name().to_str() {
                    Some(v) => v.to_string(),
                    None => continue,
                };
                info!("Checking regex of {:?}", filename);
                if re.is_match(&filename) {
                    info!("Matched: {:?}", filename);
                    let mut stmt = db
                        .prepare("SELECT uuid FROM files WHERE uuid = :uuid")
                        .expect("Failed to prepare SELECT statement");
                    let mut rows = stmt
                        .query_named(named_params! {
                            ":uuid": &filename,
                        })
                        .expect("Error querying rows");
                    let record = match rows.next() {
                        Ok(v) => v,
                        Err(_) => panic!("Error checking if entry exists"),
                    };
                    // if (uuid not in database)
                    info!("File in database {:?}", record.is_some());
                    if record.is_none() {
                        let remove_status = remove_file(entry.path());
                        if remove_status.is_err() {
                            warn!("Error deleting file");
                        }
                    }
                }
            }
        }
    } else if let Some(ref matches) = matches.subcommand_matches("print") {
        let db = dir.join("upload.db3");
        let db = Connection::open(db).expect("Failed to create database");
        let mut stmt = db
            .prepare("SELECT uuid, filename, state, ctime, mtime FROM files")
            .expect("Failed to prepare SELECT statement");

        struct Entry {
            uuid: String,
            filename: String,
            state: String,
            ctime: String,
            mtime: String,
        }
        let rows = stmt
            .query_map(NO_PARAMS, |row| {
                Ok(Entry {
                    uuid: row.get(0)?,
                    filename: row.get(1)?,
                    state: row.get(2)?,
                    ctime: row.get(3)?,
                    mtime: row.get(4)?,
                })
            })
            .expect("Failed to read rows");
        for row in rows {
            if let Ok(r) = row {
                if matches.is_present("age") {
                    let key = matches.value_of("age").expect("Couldn't get age key path");
                    let filepath = dir.join(r.uuid);
                    if r.state == UploadStatus::Finished.value() {
                        println!("age -d -i {:?} -o {:?} {:?}", key, r.filename, filepath)
                    }
                } else {
                    println!(
                        "{}:{:#?}\n\t{:#?}\n\tstart:{:#?} end:{:#?}",
                        r.uuid, r.state, r.filename, r.ctime, r.mtime
                    );
                }
            } else {
                println!("Error reading row")
            }
        }
    } else {
        println!("Nothing to do, see usage below\n\n{}", matches.usage());
    }
}
