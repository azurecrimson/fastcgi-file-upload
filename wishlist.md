### Request Body
- HTTP/non-fastcgi support
    - Low priority
    - PRs welcome ;)
- Make header parsing RFC 7578 compliant
    - In progress
    - Very complicated format
        - https://tools.ietf.org/html/rfc7578#section-4
        - https://tools.ietf.org/html/rfc2183#section-2
        - https://tools.ietf.org/html/rfc2045#section-5.1
        - https://tools.ietf.org/html/rfc822#section-3.4.3
        - https://github.com/Geal/nom/blob/master/doc/choosing_a_combinator.md
- ~~Multiple files per multipart/form-data~~ Implemented

### General
- ~~Clap subcommand (command line usage change)~~ Implemented
    - Important before adding more features
- Logging support
    - Important for debugging
    - https://docs.rs/log/0.4.10/log/
        - https://docs.rs/log/0.4.10/log/fn.set_max_level.html
- Test suite
    - Especially for parsing code
    - Payload generation is difficult though
- ~~Stable database layout~~
- ~~Querying uploaded files~~ Anti-feature (candidate for PR if optional)
    - Store plaintext hash
        - Client-side query
        - Server-side auto-dedup
        - Information leak
    - Store HMAC with password
        - Client-side query
        - Indirectly stores password
        - Minor information leak
    - Possibly an anti-feature
        - Depends on why you're encrypting
- Limits
    - LRU eviction
    - Largest eviction
    - No new uploads
    - Per-upload cap
- Decryption
    - Partially implemented
- Graceful Shutdown
    - https://rust-cli.github.io/book/in-depth/signals.html
    - https://vorner.github.io/2018/06/28/signal-hook.html
    - SIGUSR1, atomic global boolean, check reader count on exit
- ~~Multiple upload support~~ Implemented
- Extrace support
    - Extract to directory as uuid.decrypted
    - Then chroot/pivot_root
    - Then rename

### Bugs
- Ctrl+C leaves database rows in "Writing" state

### Performance
- ~~KMP string search~~ (Implemented)
- Profiling
    - Standardize with testing suite
    - Can already test using database start/finish times

### Housekeeping
- ~~Add option for cleaning failed xor working state files~~
- Signal to finish working state requests and stop
- Periodically clean failed uploads (default, automatic)